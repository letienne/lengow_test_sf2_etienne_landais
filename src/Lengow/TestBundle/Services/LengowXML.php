<?php

namespace Lengow\TestBundle\Services;

use Monolog\Logger;

/**
 * Gets XML flux from urlOrders
 */
class LengowXML {

    private $logger;
    private $urlOrders;
    private $curlResource;

    function __construct(Logger $logger, $urlOrders) {
        $this->logger = $logger;
        $this->urlOrders = $urlOrders;
    }

    /**
     * gets XML
     * @return String
     */
    public function get() {
        $this->logger->info("Retrieve xml file at defined adress : " . $this->urlOrders);
        

        return $this->getContent();
    }

    /**
     * CURL call
     * @return type
     */
    private function getContent() {

        $this->initCurlResource();
        curl_setopt($this->curlResource, CURLOPT_URL, $this->urlOrders);
        curl_setopt($this->curlResource, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($this->curlResource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curlResource, CURLOPT_HEADER, 0);
        curl_setopt($this->curlResource, CURLOPT_FOLLOWLOCATION, 1);
        
        $this->logger->info("HTTP return code : " .(curl_getinfo($this->curlResource, CURLINFO_HTTP_CODE))); // HTTP return code
        $this->logger->info("CURL error code : " .(curl_errno($this->curlResource))); // CURL error code
        
        $xml = curl_exec($this->curlResource);
        
        $this->closeCurlRessource();
        
        return $xml;
        
    }

    /**
     * init CURL resource
     */
    private function initCurlResource() {
        if ($this->curlResource) {
            $this->closeCurlRessource($this->curlResource);
        }
        $this->curlResource = curl_init();
        
    }

    /**
     * destroy CURL resource
     */
    private function closeCurlRessource() {
        curl_close($this->curlResource);
        $this->curlResource = null;
    }

}
