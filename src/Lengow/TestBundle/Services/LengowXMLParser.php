<?php

namespace Lengow\TestBundle\Services;

use Monolog\Logger;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Parser for the lengow XML flux
 */
class LengowXMLParser {

    private $logger;
    private $doctrine;
    private $xml;

    function __construct(Logger $logger, Registry $doctrine, $xml) {
        $this->logger = $logger;
        $this->doctrine = $doctrine;
        $this->xml = $xml;
    }

    /**
     * Parsing XML using XPATH request
     */
    public function parse() {
        // Retrieve XML flux using LengowXML service
        $xmlFlux = new \SimpleXMLElement($this->xml->get());

        $orders = $xmlFlux->xpath('orders/order');
        foreach ($orders as $order) {
            $orderArray = Array();
            $orderArray["marketplace"] = $order->marketplace->__toString();
            $orderArray["order_id"] = $order->order_id->__toString();
            $orderArray["order_tax"] = $order->order_tax->__toString();
            $orderArray["order_amount"] = $order->order_amount->__toString();
            $this->save($orderArray);
        }
    }

    /**
     * Save Order from XML into DB using Doctrine
     * @param array $order
     */
    private function save(array $order) {
        $em = $this->doctrine->getEntityManager();
        //check if the order already exists 
        $lengowOrder = $em->getRepository('LengowTestBundle:LengowOrder')->findByOrderId($order["order_id"]);
        //if not we save it in DB
        if (!$lengowOrder) {
            $lengowOrder = new \Lengow\TestBundle\Entity\LengowOrder();
            $lengowOrder->setMarketPlace($order["marketplace"]);
            $lengowOrder->setOrderId($order["order_id"]);
            $lengowOrder->setOrderTax($order["order_tax"]);
            $lengowOrder->setOrderAmount($order["order_amount"]);
            $em->persist($lengowOrder);
            $em->flush();
        }
    }

}
