<?php

namespace Lengow\TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LengowOrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('marketPlace')
            ->add('orderId')
            ->add('orderAmount')
            ->add('orderTax')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lengow\TestBundle\Entity\LengowOrder'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lengow_testbundle_lengoworder';
    }
}
