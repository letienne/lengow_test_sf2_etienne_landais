<?php

namespace Lengow\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * LengowOrder
 *
 * @ORM\Table()
 * @ORM\Entity
 * @GRID\Source(columns="id, orderId, marketPlace, orderAmount, orderTax ")
 * @UniqueEntity(fields="orderId", message="La commande existe déja")
 * @ORM\Entity(repositoryClass="Lengow\TestBundle\Entity\LengowOrderRepository")
 */
class LengowOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="marketPlace", type="string", length=255)
     */
    private $marketPlace;

    /**
     * @var string
     *
     * @ORM\Column(name="orderId", type="string", length=255, unique=true)
     */
    private $orderId;

    /**
     * @var float
     *
     * @ORM\Column(name="orderAmount", type="float")
     */
    private $orderAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="orderTax", type="float")
     */
    private $orderTax;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketPlace
     *
     * @param string $marketPlace
     * @return LengowOrder
     */
    public function setMarketPlace($marketPlace)
    {
        $this->marketPlace = $marketPlace;

        return $this;
    }

    /**
     * Get marketPlace
     *
     * @return string 
     */
    public function getMarketPlace()
    {
        return $this->marketPlace;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return LengowOrder
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set orderAmount
     *
     * @param float $orderAmount
     * @return LengowOrder
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return float 
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * Set orderTax
     *
     * @param float $orderTax
     * @return LengowOrder
     */
    public function setOrderTax($orderTax)
    {
        $this->orderTax = $orderTax;

        return $this;
    }

    /**
     * Get orderTax
     *
     * @return float 
     */
    public function getOrderTax()
    {
        return $this->orderTax;
    }
}
