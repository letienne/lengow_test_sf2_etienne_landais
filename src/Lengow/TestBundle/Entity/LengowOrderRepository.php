<?php

namespace Lengow\TestBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query;

/**
 * LengowOrder Repository
 */
class LengowOrderRepository extends EntityRepository {

    /**
     * Find all LengowOrder formatted to array
     * @return Array
     */
    public function allArray(){
           $q = $this->_em->createQueryBuilder()
                ->select('lo')
                ->from('LengowTestBundle:LengowOrder', 'lo');
           
           return $q->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
    
    /**
     * Find one LengowOrder formatted to array
     * @param string $orderId
     * @return Array
     */
    public function oneArray($orderId){
           $q = $this->_em->createQueryBuilder()
                ->select('lo')
                ->from('LengowTestBundle:LengowOrder', 'lo')
                ->where('lo.orderId = :orderId')
                 ->setParameter('orderId', $orderId);
           
           return $q->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);
    }
}

