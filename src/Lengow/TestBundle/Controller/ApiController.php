<?php

namespace Lengow\TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lengow\TestBundle\Entity\LengowOrder;
use Doctrine\ORM\Query;
use Symfony\Component\Yaml\Yaml;

/**
 * Api controller.
 *
 * @Route("/api")
 */
class ApiController extends Controller {

    /**
     * all LengowOrder entities JSON.
     *
     * @Route("/orders/{format}",name="retrieveall",defaults={"format" = "json"})
     * @Method("GET")
     * @Template()
     */
    public function RetrieveAllAction($format) {
        $em = $this->getDoctrine()->getManager();

        $orders = $em->getRepository('LengowTestBundle:LengowOrder')->allArray();


        if ($format === "json") {
            $response = new Response(json_encode($orders));
            $response->headers->set('Content-Type', 'application/json');
        } else if ($format === "yaml") {
            $response = new Response(Yaml::dump($orders, 2));
            $response->headers->set('Content-Type', 'application/yaml');
        } else {
            $response = new Response('format not supported');
        }

        return $response;
    }

    /**
     * One LengowOrder entities JSON.
     *
     * @Route("/order/{orderId}/{format}", name="retrieveone",defaults={"format" = "json"})
     * @Method("GET")
     * @Template()
     */
    public function RetrieveOneAction($format, $orderId) {
        $em = $this->getDoctrine()->getManager();

        $order = $em->getRepository('LengowTestBundle:LengowOrder')->oneArray($orderId);

        if ($format === "json") {
            $response = new Response(json_encode($order));
            $response->headers->set('Content-Type', 'application/json');
        } else if ($format === "yaml") {
            $response = new Response(Yaml::dump($order, 2));
            $response->headers->set('Content-Type', 'application/yaml');
        } else {
            $response = new Response('format not supported');
        }

        return $response;
    }
    
    /**
     * Save Orders from XML
     *
     * @Route("/xml", name="saveorders")
     * @Method("GET")
     * @Template()
     */
    public function saveOrdersAction() {
        
        $this->get("lengow_test.xmlparser")->parse();
        
        $response = new Response('See logs for more informations');
        return $response;
    }
}
