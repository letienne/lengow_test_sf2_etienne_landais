<?php

namespace Lengow\TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lengow\TestBundle\Entity\LengowOrder;
use Lengow\TestBundle\Form\LengowOrderType;
use APY\DataGridBundle\Grid\Source\Entity;

/**
 * LengowOrder controller.
 *
 * @Route("/lengoworder")
 */
class LengowOrderController extends Controller
{

    /**
     * Lists all LengowOrder entities.
     *
     * @Route("/", name="lengoworder")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        // Creates simple grid based on your entity (ORM)
        $source = new Entity('LengowTestBundle:LengowOrder');

        // Get a grid instance
        $grid = $this->get('grid');

        // Attach the source to the grid
        $grid->setSource($source);

        // Configuration of the grid
        // Manage the grid redirection, exports and the response of the controller
        return $grid->getGridResponse('LengowTestBundle::grid.html.twig');
    }
    /**
     * Creates a new LengowOrder entity.
     *
     * @Route("/", name="lengoworder_create")
     * @Method("POST")
     * @Template("LengowTestBundle:LengowOrder:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new LengowOrder();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('lengoworder', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a LengowOrder entity.
     *
     * @param LengowOrder $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(LengowOrder $entity)
    {
        $form = $this->createForm(new LengowOrderType(), $entity, array(
            'action' => $this->generateUrl('lengoworder_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new LengowOrder entity.
     *
     * @Route("/new", name="lengoworder_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new LengowOrder();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
}
